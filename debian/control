Source: postfix
Section: mail
Priority: optional
Maintainer: Debian Postfix Team <team+postfix@tracker.debian.org>
Uploaders: LaMont Jones <lamont@debian.org>,
           Michael Tokarev <mjt@tls.msk.ru>,
           Scott Kitterman <scott@kitterman.com>
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://www.postfix.org
Build-Depends: debhelper-compat (= 13),
               default-libmysqlclient-dev,
               groff-base,
               html2text,
               libcdb-dev,
               libdb-dev,
               libicu-dev,
               libldap-dev,
               liblmdb-dev,
# mongoc is not available on hurd, mark it linux-any for now
               libmongoc-dev [linux-any],
               libnsl-dev | libc6-dev (<<2.37-15.1),
               libpcre2-dev,
               libpq-dev,
               libsasl2-dev,
               libsqlite3-dev,
               libssl-dev,
               patch,
               pkgconf,
               po-debconf,
               txt2man
Vcs-Browser: https://salsa.debian.org/postfix-team/postfix-dev
Vcs-Git: https://salsa.debian.org/postfix-team/postfix-dev.git

Package: postfix
Architecture: any
Pre-Depends: ${misc:Pre-Depends}, init-system-helpers (>= 1.54~)
Depends: adduser,
         netbase,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: mail-transport-agent,
# RELEASE_NOTES file moved from postfix-doc, not worth a Breaks:
	postfix-doc (<<3.9.1-3~),
# manpages moved from postfix-MAP subpackages:
	postfix-ldap (<<3.9.1-5~), postfix-lmdb (<<3.9.1-5~),
	postfix-pcre (<<3.9.1-5~),
	postfix-mongodb (<<3.9.1-5~), postfix-mysql (<<3.9.1-5~),
	postfix-pgsql (<<3.9.1-5~), postfix-sqlite (<<3.9.1-5~),
Recommends:
# for smtps:
        ca-certificates,
# for snakeoil cert (used in smtpd_tls_key_file):
        ssl-cert,
# for postfix-add-filter, postfix-add-policy:
        python3,
Suggests: libsasl2-modules | dovecot-common,
          mail-reader,
          postfix-cdb,
          postfix-doc,
          postfix-ldap,
          postfix-lmdb,
          postfix-mta-sts-resolver,
          postfix-mongodb [linux-any],
          postfix-mysql,
          postfix-pcre,
          postfix-pgsql,
          postfix-sqlite,
          procmail,
          resolvconf,
          sasl2-bin | dovecot-common,
          ufw
Conflicts: mail-transport-agent, smail
Provides: mail-transport-agent, ${postfix:Provides}
Description: High-performance mail transport agent
 Postfix is Wietse Venema's mail transport agent that started life as an
 alternative to the widely-used Sendmail program.  Postfix attempts to
 be fast, easy to administer, and secure, while at the same time being
 sendmail compatible enough to not upset existing users.  Thus, the outside
 has a sendmail-ish flavor, but the inside is completely different.

Package: postfix-doc
Architecture: all
Section: doc
Suggests: postfix
Depends: ${misc:Depends}
Description: Documentation for Postfix
 Postfix is Wietse Venema's mail transport agent, available in Debian
 in postfix package.
 .
 This package provides documentation for Postfix.

Package: postfix-ldap
Architecture: any
Depends: postfix (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: LDAP map support for Postfix
 Postfix is Wietse Venema's mail transport agent, available in Debian
 in postfix package.
 .
 This package provides support for LDAP maps in Postfix.
 If you plan to use LDAP maps with Postfix, you need this.

Package: postfix-lmdb
Architecture: any
Depends: liblmdb0 (>=0.9.14),
         postfix (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: LMDB map support for Postfix
 Postfix is Wietse Venema's mail transport agent, available in Debian
 in postfix package.
 .
 This package provides support for LMDB maps in Postfix.
 If you plan to use LMDB maps with Postfix, you need this.

Package: postfix-cdb
Architecture: any
Depends: postfix (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: CDB map support for Postfix
 Postfix is Wietse Venema's mail transport agent, available in Debian
 in postfix package.
 .
 This package provides support for CDB (constant database) maps in Postfix.
 If you plan to use CDB maps with Postfix, you need this.

Package: postfix-pcre
Architecture: any
Depends: postfix (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: PCRE map support for Postfix
 Postfix is Wietse Venema's mail transport agent, available in Debian
 in postfix package.
 .
 This package provides support for PCRE (perl-compatible regular expression)
 maps in Postfix.  If you plan to use PCRE maps with Postfix, you need this.

Package: postfix-mongodb
Architecture: linux-any
Depends: postfix (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: MongoDB map support for Postfix
 Postfix is Wietse Venema's mail transport agent, available in Debian
 in postfix package.
 .
 This package provides support for MongoDB maps in Postfix.
 If you plan to use MongDB maps with Postfix, you need this.

Package: postfix-mysql
Architecture: any
Depends: postfix (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: MySQL map support for Postfix
 Postfix is Wietse Venema's mail transport agent, available in Debian
 in postfix package.
 .
 This package provides support for MySQL maps in Postfix.
 If you plan to use MySQL maps with Postfix, you need this.

Package: postfix-pgsql
Architecture: any
Depends: postfix (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: PostgreSQL map support for Postfix
 Postfix is Wietse Venema's mail transport agent, available in Debian
 in postfix package.
 .
 This package provides support for PostgreSQL maps in Postfix.
 If you plan to use PostgreSQL maps with Postfix, you need this.

Package: postfix-sqlite
Architecture: any
Depends: postfix (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: SQLite map support for Postfix
 Postfix is Wietse Venema's mail transport agent, available in Debian
 in postfix package.
 .
 This package provides support for SQLite maps in Postfix.
 If you plan to use SQLite maps with Postfix, you need this.
