postfix (3.9.1-7) unstable; urgency=medium

  postfix@-.service has become postfix.service
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  This release reworks Systemd integration of Postfix in Debian.  For most
  common simple setups: the infamous postfix@- service, which has been in
  Debian since Stretch, is gone, replaced with regular postfix.service.
  For example, `journalctl -u postfix@-' becomes `journalctl -u postfix'.


  multi-instance postfix startup has changed under systemd
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  For multi-instance setups, the startup procedure has changed, and multiple
  instances will NOT be started automatically anymore.  Please read the
  updated /usr/share/doc/postfix/README.Debian.gz, sections "Multiple Postfix
  instances under systemd" and "Postfix and Systemd", for more information.
  If you need multiple Postfix instances and you're using systemd, please
  configure additional instances startup manually.

  The upgade procedure should dectect if multiple instances are in use and
  warn the user to review the situation.

  This does not affect non-systemd users, the updated postfix startup script
  manages all instances like the upstream designed it, with the help of
  postmulti wrapper tool (see MULTI_INSTANCE_README file in the postfix-doc
  package for more information).

 -- Michael Tokarev <mjt@tls.msk.ru>  Fri, 13 Dec 2024 20:12:06 +0300
