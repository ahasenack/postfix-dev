From: Michael Tokarev <mjt@tls.msk.ru>
Date: Fri, 20 Dec 2024 22:48:39 +0300
Subject: implement "postfix chroot" command
Forwarded: no

Implement `postfix chroot' command to assist chrooting and un-chrooting
postfix services in master.cf.  The command always ignores postfix services
which should never be chrooted, and by default (unless -c option is specified)
also ignores custom (unknown, unrecognized) services.

  postfix chroot [-SXc] query|q - print on|off|mixed
  postfix chroot [-SXc] - the same as query
  postfix chroot [-nSXc] on|y - set services to run chrooted
  postfix chroot [-nSXc] off|n - set services to run non-chrooted

  -n - do not make actual changes, only show what would be done
  -S - omit known to be simple/safe services (most internal postfix)
  -X - omit known to be complex/unsafe services
  -c - also act on unknown/unrecognized services (which are normally
       skipped)

The more complex ones are basically lmtp, smtp, smtpd, which might need
TLS certificates, access to additional SASL data and the like.  Some
"safe" ones might include a few files in chroot, like /etc/hosts,
/etc/localtime, etc.

diff --git a/conf/postfix-script b/conf/postfix-script
--- a/conf/postfix-script
+++ b/conf/postfix-script
@@ -409,4 +409,73 @@ post-install)
 	;;
 
+chroot) # debian-specific
+	shift
+	r=`$command_directory/postconf -h compatibility_level` || exit
+	if expr $r : '\([0-9]*\)' \< 3 >/dev/null
+	then yes='[-y]' no='[n]' # postfix  <3.0, - means y
+	else yes='[y]' no='[-n]' # postfix >=3.0, - means n
+	fi
+	while getopts nSXc r; do case "$r" in
+	    n) show=echo;; # do not make changes, only show
+	    S) nosimple=y;; # omit known simple/safe services
+	    X) nocomplex=y;; # omit known complex/unsafe services
+	    c) custom=y;; # also apply to custom commands if any
+	    *) exit 1;;
+	esac; done
+	shift `expr ${OPTIND} - 1`
+	case "$1" in
+	    on | y) verify="$yes" set=y ;;
+	    off | n) verify="$no"  set=n ;;
+	    query | q | "") verify="$yes" set= ;;
+	    *) echo "E: chroot: unknown arg $1 (expected [-n][-S][-X][-c] on|off|query)" >&2
+	       exit 1
+	    ;;
+	esac
+	r=`$command_directory/postconf -M |
+	  { while read name type x x chroot x x cmd x; do
+		case $cmd in
+
+		    local|pipe|postlogd|proxymap|spawn|virtual)
+			continue ;; # ignore non-chrootable ones
+
+		    anvil|bounce|cleanup|discard|dnsblog|error|flush|pickup| \
+		    postscreen|scache|showq|tlsmgr|tlsproxy|trivial-rewrite| \
+		    verify|nqmgr|oqmgr|qmgr|qmqpd)
+			[ -n "$nosimple" ] && continue;;
+
+		    lmtp|smtp|smtpd)
+			[ -n "$nocomplex" ] && continue;;
+
+		    *) # a custom/unknown/unrecognized command
+			if [ -z "$custom" ]; then
+			   echo "W: custom service $name/$type/command=$cmd (ignored)" >&2
+			   continue
+			fi
+			;;
+
+		esac
+		case "$chroot" in
+		    $verify) match=y ;;
+		    *) nomatch=y; [ -n "$set" ] && echo $name/$type/chroot=$set ;;
+		esac
+	    done
+	    if [ -z "$set" ]; then # query only
+		case "$match:$nomatch" in
+		    y:)  echo on ;;
+		    :y)  echo off ;;
+		    y:y) echo mixed ;;
+		    :)   echo off ;;
+		esac
+	    fi
+	  }
+	`
+	if [ -z "$set" ]; then # query only
+	    echo $r
+	elif [ -n "$r" ]; then # something should be changed
+	    $show $command_directory/postconf -c $config_directory \
+		-F $r
+	fi
+	;;
+
 tls)
 	shift
diff --git a/src/postfix/postfix.c b/src/postfix/postfix.c
--- a/src/postfix/postfix.c
+++ b/src/postfix/postfix.c
@@ -91,4 +91,27 @@
 /* .sp
 /*	This feature is available in Postfix 3.4 and later.
+/* .IP "\fBchroot\fR [\fB-n\fR] [\fB-SXc\fR] [\fBon\fR|\fBy\fR|\fBoff\fR|\fBn\fR|\fBquery\fR|\fBq\fR] (Debian-specific)"
+/*	Control chroot column settings in master.cf file for services known
+/*	to be able to work in postfix chroot jail.  \fBon\fR or \fBy\fR
+/*	enables chroot, \fBoff\fR or \fBn\fR disables chroot, and \fBquery\fR
+/*	or \fBq\fR (or no argument) shows the current status, which might
+/*	be \fBon\fR, \fBoff\fR, or \fBmixed\fR (if some services are chrooted
+/*	while some are not).
+/* .sp
+/*	With \fB-n\fR, show what would be done instead of applying changes.
+/* .br
+/*	With \fB-S\fR, omit safe/simple services, for which either no or very
+/*	minor chroot setup is necessary (usually just /etc/localtime and/or
+/*	/etc/hosts is needed in the chroot jail).
+/* .br
+/*	With \fB-X\fR, omit more complex services, which might requre more
+/*	complex chroot setup (\fBlmtp\fR(8), \fBsmtp\fR(8), \fBsmtpd\fR(8)).
+/* .br
+/*	With \fB-c\fR, also include custom (unknown, unrecognized) services,
+/*	which are normally skipped.
+/* .sp
+/*	Please note: this command only modifies \fBmaster.cf\fR, it does not
+/*	prepare/update the actual chroot jail.
+/*
 /* .IP "\fBtls\fR \fIsubcommand\fR"
 /*	Enable opportunistic TLS in the Postfix SMTP client or
