From: Michael Tokarev via Postfix-users <postfix-users@postfix.org>
Date: Fri, 13 Dec 2024 07:56:08 +0300
Subject: makedefs: fix $RELEASE_MAJOR expression
Forwarded: https://marc.info/?l=postfix-users&m=173406561120227&w=2

There are 2 issues with the way RELEASE_MAJOR is currently
computed in ./makedefs.  First, it is not set at all when
the system name/release are specified on the command line,
so this change moves it a few lines down.

And second, the usage of "expr" utility is wrong, as it does
not work when the system release is 0.something.  Consider:

  expr 0.foo : '\([0-9]*\)'

the ":" expression itself will return the first N digits,
which is "0" in this case.  But the less widely known thing
about expr is that it works with numbers, not strings.
So this becomes:

  expr 0

which, in turn, is false.  So while expr utility will produce
"0" on output, it will ALSO exit with non-zero status.  And the
next "exit 1" immediately gets in, so whole makedefs terminates.

Fix this by using sed instead of expr.

Introduced in 3.0.2.

Signed-off-by: Michael Tokarev <mjt@tls.msk.ru>
---
 makedefs | 5 +++--
 1 file changed, 3 insertions(+), 2 deletions(-)

diff --git a/makedefs b/makedefs
index 1932e36d..8a2120b8 100644
--- a/makedefs
+++ b/makedefs
@@ -239,8 +239,6 @@ case $# in
  # Officially supported usage.
  0) SYSTEM=`(uname -s) 2>/dev/null`
     RELEASE=`(uname -r) 2>/dev/null`
-    # No ${x%%y} support in Solaris 11 /bin/sh
-    RELEASE_MAJOR=`expr "$RELEASE" : '\([0-9]*\)'` || exit 1
     VERSION=`(uname -v) 2>/dev/null`
     case "$VERSION" in
      dcosx*) SYSTEM=$VERSION;;
@@ -250,6 +248,9 @@ case $# in
  *) echo usage: $0 [system release] 1>&2; exit 1;;
 esac
 
+# No ${x%%y} support in Solaris 11 /bin/sh
+RELEASE_MAJOR=`echo "$RELEASE" | sed 's/[^0-9].*//'` || exit 1
+
 case "$SYSTEM.$RELEASE" in
    SCO_SV.3.2)	SYSTYPE=SCO5
 		# Use the native compiler by default
-- 
2.39.5

